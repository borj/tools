true;API server maps anonymous requests to the system:unauthenticated group;curl -kv https://<master>:8443/api/v1/services|grep '403 Forbidden'
false;Maintain default behavior for anonymous access - anonymous;oc get clusterrolebindings | grep system:anonymous
true;Maintain default behavior for anonymous access - authenticated;oc get clusterrolebindings | grep system:unauthenticated|grep -Ev 'self-access-reviewer|system:oauth-token-deleter|cluster-status|system:webhook|system:discovery|system:scope-impersonation|system:basic-user'
