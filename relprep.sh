#!/usr/bin/env bash

# Create release directory, edit the parameters file and upload it
# * NOTE: curl is required
# boris.yordanov@dxc.com

# Define some color escape codes for the colored output
BLUE='\e[34m'
YELLOW='\e[33m'
RED='\e[31m'
RESET='\e[0m' # Resets the formating to default

CURUSER="${USER%@*}"
PREPARED="FALSE"
RELEASEPREFFIX=""

helpFunction()
# Print help and usage
{
  echo ""
  echo "Usage: $0 <-e> Environment <-r> Release number (r**********) [-p] Download an already prepared properties file from release repository"
  echo "Valid environments are: sit|sit-gdv|uat|uat-gdv|uat-trx|prod|prod-gdv"
  echo ""
  exit 1 # Exit script after printing help
}

# Check and parse input parameters
while getopts :r:e:p option
do
  case "${option}"
    in
    r) RELEASENUMBER="$OPTARG";;
    e) ENVIRONMENT="$OPTARG";;
    p) PREPARED="TRUE";;
    \?) helpFunction ;; # If a non-existent option is provided - print help
  esac
done

# Check if input is provided
if [ -z "$ENVIRONMENT" ]
then
  echo "-e is required";
  helpFunction
fi
if [[ ! "$ENVIRONMENT" =~ ^(sit|sit-gdv|uat|uat-gdv|uat-trx|prod|prod-gdv)$ ]]; then
    echo -e "${RED}"
    echo "$ENVIRONMENT is not a valid environment!"
    echo  -e "${RESET}"
    helpFunction
fi
if [ -z "$RELEASENUMBER" ]
then
  echo "-r is required";
  helpFunction
fi

echo "Current user is $CURUSER"

cd /var/release

#Different SRCFILE, DSTFILE, UPLOADURL, DEPLOYMENT allow for a more flexible operation
case "$ENVIRONMENT" in
    sit)
        SRCFILE="parameters.properties.sit.$RELEASENUMBER"
        DESTFILE="parameters.properties.sit.$RELEASENUMBER"
        DEPLOYMENT="sit-all-avq-apps"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"
        ;;
    sit-gdv)
        SRCFILE="values-sit-$RELEASENUMBER.yaml"
        DESTFILE="values-sit-$RELEASENUMBER.yaml"
        DEPLOYMENT="sit-gdv-bestandsanzeige"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"
        RELEASEPREFFIX="gdv-"
        ;;
    uat)
        SRCFILE="parameters.properties.uat.$RELEASENUMBER"
        DESTFILE="parameters.properties.uat.$RELEASENUMBER"
        DEPLOYMENT="uat-all-avq-apps"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"
        ;;
    uat-gdv)
        SRCFILE="values-uat-$RELEASENUMBER.yaml"
        DESTFILE="values-uat-$RELEASENUMBER.yaml"
        DEPLOYMENT="uat-gdv-bestandsanzeige"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"
        RELEASEPREFFIX="gdv-"
        ;;
    uat-trx)
        SRCFILE="parameters.properties.uat-trx.$RELEASENUMBER"
        DESTFILE="parameters.properties.uat-trx.$RELEASENUMBER"
        DEPLOYMENT="uat-trx-all-avq-apps"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"    
        RELEASEPREFFIX="trx-"
        ;;
    prod)
        SRCFILE="parameters.properties.prod.$RELEASENUMBER"
        DESTFILE="parameters.properties.prod.$RELEASENUMBER"
        DEPLOYMENT="prod-all-avq-apps"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"
        ;;
    prod-gdv)
        SRCFILE="values-prod-$RELEASENUMBER.yaml"
        DESTFILE="values-prod-$RELEASENUMBER.yaml"
        DEPLOYMENT="prod-gdv-bestandsanzeige"
        UPLOADURL="https://nexus.apobank.lan/repository/raw-dxc-release/$DEPLOYMENT/$RELEASENUMBER/"
        RELEASEPREFFIX="gdv-"
        ;;
esac

if [ ! -d "$RELEASEPREFFIX$RELEASENUMBER" ]
then
    echo -e "${YELLOW}"
    echo "Creating directory" $RELEASEPREFFIX$RELEASENUMBER
    echo  -e "${RESET}"
    mkdir $RELEASEPREFFIX$RELEASENUMBER
   #exit
fi

cd $RELEASEPREFFIX$RELEASENUMBER

#Fetch the properties file prepared by the Jenkins task
if [ "$PREPARED" == "TRUE" ]
then
  echo -e "${YELLOW}"
  echo "Fetching parameters file " $UPLOADURL$DESTFILE
  echo  -e "${RESET}"
  curl --fail --user "$CURUSER" -O "$SRCFILE" "$UPLOADURL$DESTFILE"
fi

#Edit the file
vim "$SRCFILE"

#Upload edited file
curl --user "$CURUSER" --upload-file "$SRCFILE" "$UPLOADURL""$DESTFILE"