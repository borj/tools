#!/bin/sh
# Collects RSA host keys for a SSH known_hosts file
# requires input host list (hostnames only, one host per line),
# and domain to be specified
# will create entries (if possible) for host, host.domain and hostIP
# Default values are as follows:


infile=hostlist.txt
outfile=known_hosts.txt
domain="apobank.lan"

usage() {
    echo "Usage: $0 [ -i INFILE ] [ -o OUTFILE ] [ -d DOMAIN ]" 1>&2
    echo "Example (defaults): $0 -i $infile -o $outfile -d $domain" 1>&2
}

exit_abnormal() {
    usage
    exit 1
}

while getopts ":i:o:d:" options; do
    
    case "${options}" in
        i)
            infile=${OPTARG}
        ;;
        o)
            outfile=${OPTARG}
        ;;
        d)
            domain=${OPTARG}
        ;;
        :)                                    # If expected argument omitted:
            echo "Error: -${OPTARG} requires an argument."
            exit_abnormal                       # Exit abnormally.
        ;;
        *)                                    # If unknown (any other) option:
            exit_abnormal                       # Exit abnormally.
        ;;
    esac
done

while IFS= read -r line
do
    ssh-keyscan -t rsa $line >> $outfile
    ssh-keyscan -t rsa $line.$domain >> $outfile
    HOSTIP=$(host $line.$domain|awk '{print $4}')
    ssh-keyscan -t rsa $HOSTIP >> $outfile
done < "$infile"
