#!/bin/bash

# Cluster where the script is being executed is considered "Cluster 1" and the other one - "Cluster 2"
# WARNING - execute on the cluster that was offline!!!
# Checks all portworx volumes used by $NSTARGET. If all the volume replicas are located on Cluster 2 the script will add
# an additional replica on Cluster 1. Once all volumes are processed the script will wait for their
# repplication status to become Up and will then remove one replica from Cluster 2

#IMPORTANT VARIABLES
#Targeted namespace (used for volume selection)
NSTARGET="namespace=sit-"


#A key=value array holding storagenodeIP=storagenodeID pairs. For Storage node ID lookups.
declare -A PX_NODEIDLIST
#A key=value array holding Index=storagenodeIP pairs. For Storage node IP lookups.
declare -A PX_NODEIPLIST
# Only process volumes with HA_COUNT replicas
HA_COUNT=2
#Node ID enumerator
NODEIDCOUNTER=0

#Check if logged in OpenShift
oc whoami
OC_LOGIN_STATUS=$?
if [ $OC_LOGIN_STATUS -eq 0 ]; then
    echo "oc login OK"
    echo
else
    echo "Please do an 'oc login' first! Exiting."
    exit 1
fi

#Get the first PX cluster pod
PX_POD=$(oc get pods -l name=portworx -n portworx -o jsonpath='{.items[0].metadata.name}')
echo "Using storage pod $PX_POD"
echo

#Get PX storage node IDs
PX_NODELIST=$(oc exec "$PX_POD" -c portworx -n portworx -- /opt/pwx/bin/pxctl cluster list|grep Online|awk '{printf "%s %s\n",$1,$3}')
while IFS=" " read -r NODEID NODEIP
do
    #    echo $NODEIP $NODEID
    PX_NODEIDLIST[$NODEIP]=$NODEID
done < <(printf '%s\n' "$PX_NODELIST")
#echo ${PX_NODEIDLIST[10.140.34.29]}
#echo "Array size ${#PX_NODEIDLIST[@]}"

#Get storage nodes IPs
PX_NODEIP=$(oc get nodes -o wide|grep storage|awk '{print $6}')
echo "local storage node IPs:"
#echo  "$PX_NODEIP"
while IFS= read -r PX_LINE
do
    #    if [[ "$NODEIDCOUNTER" == 3 ]]; then
    #        NODEIDCOUNTER=0
    #    fi
    PX_NODEIPLIST[$NODEIDCOUNTER]=$PX_LINE
    echo "$PX_LINE"
    (( NODEIDCOUNTER+=1 ))
    
done < <(printf '%s\n' "$PX_NODEIP")
echo
#echo "Array size ${#PX_NODEIPLIST[@]}"

#Get list of volumes
PX_VOLIDLIST=$(oc exec "$PX_POD" -c portworx -n portworx -- /opt/pwx/bin/pxctl volume list|awk '{print $2}'|awk 'NR!=1 {print}')
echo "Volume ID list:"
echo "$PX_VOLIDLIST"
echo

#Process volumes one by one
NODEIDCOUNTER=0

#Iterate over volume IDs
while IFS= read -r PX_VOLID
do
    echo "========"
    echo "Volume ID $PX_VOLID has replicas on the following nodes:"
    PX_VOLDETAILS=$(oc exec "$PX_POD" -c portworx -n portworx -- /opt/pwx/bin/pxctl volume inspect "$PX_VOLID")
    #PX_VOLREPIPS=$(oc exec "$PX_POD" -c portworx -n portworx -- /opt/pwx/bin/pxctl volume inspect "$PX_VOLID"|grep Node|awk '{print $3}')
    PX_VOLREPIPS=$(echo "$PX_VOLDETAILS"|grep Node|awk '{print $3}')
    echo "$PX_VOLREPIPS"
    
    #Flag if there is a replica on a local IP
    HASLOCALREPLICA=0
    
    #Iterate over volume replica IPs
    while IFS= read -r IP
    do
        #Iterate over local storage nodes IPs
        while IFS= read -r SNIP
        do
            #If a single replica IP is found to be in the list of local storage node IPs
            #then flag the volume as OK, it has a local replica
            if [[ "$IP" == "$SNIP" ]]; then
                HASLOCALREPLICA=1
                #echo "Replica IP $IP - storage node IP $SNIP"
            fi
        done < <(printf '%s\n' "$PX_NODEIP")
    done < <(printf '%s\n' "$PX_VOLREPIPS")
    
    if [[ $HASLOCALREPLICA == 1 ]]; then
        echo "Volume has a local replica. Not processing."
    else
        #Check if the volume belongs to a DR enabled namespace (those starting with sit-)
        if [[ "$PX_VOLDETAILS" == *"$NSTARGET"* ]]; then
            echo "+++ Volume has NO local replica AND belongs to a targeted namespace. Processing. +++"
            
            if [[ "$(echo "$PX_VOLDETAILS"|grep HA|awk '{print $3}')" == "$HA_COUNT" ]]; then
                echo "Volume has the required $HA_COUNT replicas."
            else
                echo "Volume DOES NOT have the required number of $HA_COUNT replicas! Exiting."
                exit 0
            fi
            
            if [[ "$(echo "$PX_VOLDETAILS"|grep "Replication Status"|awk '{print $4}')" == "Up" ]]; then
                echo "Volume replication status is 'Up'."
            else
                echo "Volume replication status is not 'Up'! Exiting."
                exit 0
            fi
            
            if [[ "$NODEIDCOUNTER" == 3 ]]; then
                NODEIDCOUNTER=0
                echo "NodeID counter: " $NODEIDCOUNTER
            fi
            
            echo "Update command would be: oc exec $PX_POD -c portworx -n portworx -- /opt/pwx/bin/pxctl volume ha-update $PX_VOLID --repl 3 --node ${PX_NODEIDLIST[${PX_NODEIPLIST[$NODEIDCOUNTER]}]}"
            #oc exec $PX_POD -c portworx -n portworx -- /opt/pwx/bin/pxctl volume ha-update $PX_VOLID --repl 3 --node ${PX_NODEIDLIST[${PX_NODEIPLIST[$NODEIDCOUNTER]}]}"
            (( NODEIDCOUNTER+=1 ))
        else echo "Volume HAS NO local replica but does not belong to a targeted namespace. Not processing."
        fi
    fi
done < <(printf '%s\n' "$PX_VOLIDLIST")