#!/bin/bash
lvresize -r -L 5G /dev/mapper/vg00-varvol || exit 1
lvresize -r -L 5G /dev/mapper/vg00-tmpvol || exit 1
eds-linux-patch -m 3Q2020.2 -asic || exit 1
if ! grep -q ^Ciphers /etc/ssh/sshd_config ; then \
    sed -i '$s/$/\nCiphers aes128-ctr,aes192-ctr,aes256-ctr/' /etc/ssh/sshd_config; \
fi || exit 1
if ! grep -q ^MACs /etc/ssh/sshd_config; then \
    sed -i '$s/$/\nMACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160/' /etc/ssh/sshd_config; \
fi || exit 1
if ! grep -q ^kexalgorithms /etc/ssh/sshd_config; then \
    sed -i '$s/$/\nkexalgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha256,diffie-hellman-group14-sha1/' /etc/ssh/sshd_config; \
fi || exit 1
if ! grep -q ^Ciphers /etc/ssh/ssh_config; then \
    sed -i '$s/$/\nCiphers aes128-ctr,aes192-ctr,aes256-ctr/' /etc/ssh/ssh_config; \
fi || exit 1
if ! grep -q ^MACs /etc/ssh/ssh_config; then \
    sed -i '$s/$/\nMACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160/' /etc/ssh/ssh_config; \
fi || exit 1
if ! grep -q ^KexAlgorithms /etc/ssh/ssh_config; then \
    sed -i '$s/$/\nKexAlgorithms diffie-hellman-group14-sha1,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha1,diffie-hellman-group-exchange-sha256,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,curve25519-sha256,curve25519-sha256@libssh.org,gss-gex-sha1-/' /etc/ssh/ssh_config; \
fi || exit 1
if ! grep -q ^AUTOCREATE /etc/sysconfig/sshd; then \
sed -i  '/#\ AUTOCREATE_SERVER_KEYS="RSA ECDSA ED25519"/a AUTOCREATE_SERVER_KEYS="RSA"' /etc/sysconfig/sshd;
fi || exit 1
sed -i 's/^HostKey\ \/etc\/ssh\/ssh_host_ecdsa_key/#HostKey\ \/etc\/ssh\/ssh_host_ecdsa_key/' /etc/ssh/sshd_config || exit 1
sed -i 's/^HostKey\ \/etc\/ssh\/ssh_host_ed25519_key/#HostKey\ \/etc\/ssh\/ssh_host_ed25519_key/' /etc/ssh/sshd_config || exit 1
rm -f  /etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_ecdsa_key.pub /etc/ssh/ssh_host_ed25519_key /etc/ssh/ssh_host_ed25519_key.pub || exit 1
shutdown -r +1
exit 0