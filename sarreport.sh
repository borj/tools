#!/bin/bash
echo "Date, CPU average, Mem average"
for file in `ls -tr /var/log/sa/sa* | grep -v sar`
do
    sar -f $file | head -n 1 | awk '{print $4}'| tr '\n' ','
    #echo "-----------"
    sar -u -f $file | tail -n +4|awk '{printf("%.2f\n"), 100 - $9}'|head -n -1| sort -rn | head -n 1| tr '\n' ','
    sar -r -f $file | tail -n +4 |head -n -1|awk '{printf("%.2f\n"),(($4-$6-$7)/($3+$4)) * 100 }'| sort -rn | head -n 1
    #printf "\n"
done