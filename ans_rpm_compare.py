#!/usr/bin/python -tt
# skvidal - gpl blah blah blah
# compare 2 or more hosts rpm pkg lists and show differences between them


import ansible
import ansible.runner
import sys


def pkgs_from_rpmqa(rpm_out):
    ret = set([])
    for i in rpm_out.split('\n'):
        if i.strip():
            ret.add(i)
    return ret


if len(sys.argv) < 3:
    print "%s base_host other_host_specification_or_group [..]" % sys.argv[0]
    sys.exit(1)

basehost = sys.argv[1]
hostpattern = ';'.join(sys.argv[1:])
conn = ansible.runner.Runner(pattern=hostpattern, timeout=40)
conn.module_name = 'command'
conn.module_args = 'rpm -qa'

res = conn.run()

if len(res['contacted']) == 1:
    print >> sys.stderr, "Pattern resolved only one host - nothing to show"
    sys.exit(1)


if basehost not in res['contacted']:
    # taking first host in the list and calling it 'basehost'
    print >> sys.stderr, "Could not find %s in returned list - just using the first one we have" % basehost
    basehost = res['contacted'].keys()[0]

base_pkgs = pkgs_from_rpmqa(res['contacted'][basehost]['stdout'])

for host in res['contacted']:
    if host == basehost:
        continue
    host_pkgs = pkgs_from_rpmqa(res['contacted'][host]['stdout'])

    basediff = base_pkgs.difference(host_pkgs)
    hostdiff = host_pkgs.difference(base_pkgs)
    print 'Packages on %s not on %s' % (basehost, host)
    print '\n'.join(basediff)
    print ''
    print 'Packages on %s not on %s' % (host, basehost)
    print '\n'.join(hostdiff)
    print ''


if len(res['dark']):
    print >> sys.stderr, "Failed to connect to host(s)"
    for h in res['dark']:
        print h
