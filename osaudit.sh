#!/bin/bash

while IFS=';' read -r col1 col2 col3
do

        echo "Checking: $col2"

        if eval "$col3 >/dev/null 2>&1";
        then
                if [ "$col1" == "true" ];
                then
                        echo "$col2 - OK"
                else
                        echo "$col2 - FAILED"
                fi
        else
                if [ "$col1" == "false" ];
                then
                        echo "$col2 - OK"
                else
                        echo "$col2 - FAILED"
                fi
        fi

        echo""


done <auditlist.txt
